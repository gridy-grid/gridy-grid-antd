


import { GridyPagerImpl } from "../../gridy-grid/src/pager/impl/gridy-pager-impl.js";
import { SkRenderEvent } from "../../sk-core/src/event/sk-render-event.js";



export class GridyPagerAntd extends GridyPagerImpl {

    doRender() {
        this.logger.debug('GridyPagerAntd.doRender()', arguments);
        let id = this.getOrGenId();
        this.beforeRendered();
        this.renderWithVars(id);
        //this.indexMountedStyles();

        this.comp.bootstrap();

        this.afterRendered();
        this.bindEvents();
        //this.comp.bindAutoRender();
        this.comp.setupConnections();
        this.comp.callPluginHook('onRenderEnd');
        this.comp.implRenderTimest = Date.now();
        this.comp.removeFromRendering();
        this.comp.dispatchEvent(new CustomEvent('rendered', { bubbles: true, composed: true })); // :DEPRECATED
        this.comp.dispatchEvent(new SkRenderEvent({ bubbles: true, composed: true }));
        if (this.comp.renderDeferred) {
            this.comp.renderDeferred.resolve(this.comp);
        }
    }

    get pageBtnContainer() {
        if (! this._pageBtnContainer) {
            this._pageBtnContainer = this.comp.querySelector('ul');
        }
        return this._pageBtnContainer;
    }

    set pageBtnContainer(pageBtnContainer) {
        this._pageBtnContainer = pageBtnContainer;
    }

    get subEls() {
        return [ 'pageBtnContainer' ];
    }

}