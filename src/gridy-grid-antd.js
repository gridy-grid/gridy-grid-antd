

import { GridyGridImpl } from "../../gridy-grid/src/impl/gridy-grid-impl.js";



export class GridyGridAntd extends GridyGridImpl {


    get antdCssPath() {
        if (! this._antdCssPath) {
            this._antdCssPath = this.comp.confValOrDefault('dt-css-path',
                `/node_modules/gridy-grid-${this.comp.theme}/`) + 'antd.min.css';
        }
        return this._antdCssPath;
    }

    renderImpl() {
        this.logger.debug('GridyGridAntd.renderImpl()', arguments);
        super.renderImpl();
        this.comp.whenRendered(() => {
            let fileName = this.fileNameFromUrl(this.antdCssPath);
            if (!this.mountedStyles[fileName]) {
                this.attachStyleByPath(this.antdCssPath, this.comp);
            }
        });
    }
}